package com.bottlerocketstudios.com.mockitoexamples

import org.joda.time.DateTime
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
 * Created by jennifer.mcgee on 3/16/18.
 */
class MockitoDummyAnnotationExample {

    @Mock
    lateinit var dummyGradeBook: GradeBook

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    fun mockExampleWithAnnotations() {
        val underTest = SemesterServiceJava(dummyGradeBook, DateTime())
        Assert.assertEquals(0, underTest.daysSinceSemesterStart.toLong())
    }
}