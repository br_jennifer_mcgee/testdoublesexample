package com.bottlerocketstudios.com.mockitoexamples

import org.joda.time.DateTime
import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

/**
 * Created by jennifer.mcgee on 3/19/18.
 */
class MockitoVerifyExample {

    @Mock
    lateinit var stubGradeBook: GradeBook

    @Mock
    lateinit var mockEmailService: EmailService



    @Before
    fun init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    fun emailNumberOfAssignmentsForStudent() {
        //Arrange
        val studentId = 123
        val assigments = listOf<Assignment>(Assignment("1",null,null))
        Mockito.`when`(stubGradeBook.getAssignmentsForStudent(studentId)).thenReturn(assigments)
        val underTest = SemesterServiceKotlin(stubGradeBook, DateTime())
        //Act
        //mockEmailService is your mock which needs to use verify
        val result = underTest.emailNumberOfAssignmentsForStudent(studentId, mockEmailService)
        //Assert
        //Add your code here to confirm the send number of assignments is one

    }
}