package com.bottlerocketstudios.com.mockitoexamples

import org.joda.time.DateTime
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Created by jennifer.mcgee on 3/16/18.
 */
class MokitoStubWithMatcherExample {
    @Mock
    lateinit var stubGradeBook: GradeBook

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    fun getSemesterAverageForStudent_Has2AssigmentsWithGrades_ShouldReturnTheAverage() {
        //Arrange
        val assigments = listOf<Assignment>(Assignment("1", null, 100), Assignment("1", null, 0))
        Mockito.`when`(stubGradeBook.getAssignmentsForStudent(Mockito.anyInt())).thenReturn(ArrayList<Assignment>())
        val underTest = SemesterServiceKotlin(stubGradeBook, DateTime())
        //Act
        val result = underTest.getSemesterAverageForStudent(123)
        //Assert
        Assert.assertEquals(50, result)
    }
}