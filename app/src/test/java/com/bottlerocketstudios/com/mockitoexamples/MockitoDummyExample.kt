package com.bottlerocketstudios.com.mockitoexamples

import org.joda.time.DateTime
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito.mock


/**
 * Created by jennifer.mcgee on 3/16/18.
 */
class MockitoDummyExample {

    /***
     * This test is expected to fail to show that null doesn't work here.
     */
    @Test
    fun getDaysSinceSemesterStart_semesterStartsToday_shouldBeZero() {
        val today = DateTime()
        val underTest = SemesterServiceJava(null!!, today)
        assertEquals(0, underTest.daysSinceSemesterStart.toLong())
    }

    @Test
    fun Example() {
        val dummyGradeBook = mock<GradeBook>(GradeBook::class.java)

        val underTest = SemesterServiceJava(dummyGradeBook, DateTime())
        assertEquals(0, underTest.daysSinceSemesterStart.toLong())
    }


}