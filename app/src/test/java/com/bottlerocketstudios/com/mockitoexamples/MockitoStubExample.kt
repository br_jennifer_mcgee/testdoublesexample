package com.bottlerocketstudios.com.mockitoexamples

import org.joda.time.DateTime
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Created by jennifer.mcgee on 3/16/18.
 */

class SemesterServiceKotlinAverageGradeTest {
    val studentId = 1234
    @Mock
    lateinit var stubGradeBook: GradeBook

    @Before
    fun init() {
        MockitoAnnotations.initMocks(this);
    }

    /***
     * This test will not work because the stub grade book is not returning.
     */
    @Test
    fun getSemesterAverageForStudent_Has2AssigmentsWithGrades_ShouldReturnTheAverage() {
        //Arrange
        val assigments = listOf<Assignment>(Assignment("1",null,100),Assignment("1",null,0))
        //Your code here to make stubGradeBook return the list of assignments
        Mockito.`when`(stubGradeBook.getAssignmentsForStudent(studentId)).thenReturn(assigments)
        val underTest = SemesterServiceKotlin(stubGradeBook, DateTime())
        //Act
        val result = underTest.getSemesterAverageForStudent(studentId)
        //Assert
        Assert.assertEquals(50.0,result)
    }




}