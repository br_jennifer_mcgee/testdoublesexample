package com.bottlerocketstudios.com.mockitoexamples;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by jennifer.mcgee on 3/14/18.
 */
public class MockitoDummyExampleJava {

    @Test
    public void getDaysSinceSemesterStart_semesterStartsToday_shouldBeZero() throws Exception {
        DateTime today =  new DateTime();
        SemesterServiceJava underTest = new SemesterServiceJava(null, today);
        assertEquals(0,underTest.getDaysSinceSemesterStart());
    }

    @Test
    public void mockExample() throws Exception {
        GradeBook dummyGradeBook =mock(GradeBook.class);
        SemesterServiceJava underTest = new SemesterServiceJava(dummyGradeBook, new DateTime());
        assertEquals(0,underTest.getDaysSinceSemesterStart());
    }
}