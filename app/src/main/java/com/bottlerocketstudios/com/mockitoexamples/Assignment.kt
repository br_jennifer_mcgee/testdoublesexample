package com.bottlerocketstudios.com.mockitoexamples

import org.joda.time.DateTime

/**
 * Created by jennifer.mcgee on 3/18/18.
 */
data class Assignment(val name:String, val dueDate:DateTime?, val grade:Int?) {
}