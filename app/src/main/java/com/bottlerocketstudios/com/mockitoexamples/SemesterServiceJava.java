package com.bottlerocketstudios.com.mockitoexamples;

import java.util.List;

import android.support.annotation.NonNull;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.ReadableInstant;

/**
 * Created by jennifer.mcgee on 3/14/18.
 */

public class SemesterServiceJava {
    private GradeBook gradeBook;
    private ReadableInstant startDate;

    public SemesterServiceJava(@NonNull GradeBook gradeBook, DateTime start) {
        this.gradeBook = gradeBook;
        this.startDate = start;
    }

    public int getDaysSinceSemesterStart(){
        ReadableInstant today = new DateTime();
        return Days.daysBetween(startDate, today).getDays();
    }

}
