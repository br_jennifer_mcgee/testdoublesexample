package com.bottlerocketstudios.com.mockitoexamples

/**
 * Created by jennifer.mcgee on 3/18/18.
 */
data class Student(val firstName:String, val lastName:String, val studentId:Int){
}