package com.bottlerocketstudios.com.mockitoexamples

import org.joda.time.DateTime
import org.joda.time.Days

/**
 * Created by jennifer.mcgee on 3/14/18.
 */
class SemesterServiceKotlin(val gradeBook: GradeBook, val startDate: DateTime) {
    fun getDaysSinceSemesterStart(): Int {
        val today = DateTime()
        return Days.daysBetween(startDate, today).days
    }

    public fun getSemesterAverageForStudent(studentId: Int): Double? {
        val assignments = gradeBook.getAssignmentsForStudent(studentId)
        val numberOfAssigments: Double = assignments.size.toDouble()
        return assignments.map{it.grade}.requireNoNulls().sum()/numberOfAssigments
    }

    public fun getSemesterAverageForStudent(student:Student):Double?{
        return getSemesterAverageForStudent(student.studentId)
    }

    public fun emailNumberOfAssignmentsForStudent(studentId: Int, emailService:EmailService){
        val assignments = gradeBook.getAssignmentsForStudent(studentId)
        emailService.sendNumberOfAssignemnts(assignments.size)
    }
}
