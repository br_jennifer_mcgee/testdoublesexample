package com.bottlerocketstudios.com.mockitoexamples

/**
 * Created by jennifer.mcgee on 3/16/18.
 */
class GradeBook(val studentRepo:StudentRepo, val gradeRepo:GradeRepo) {

    fun getAssignmentsForStudent(studentId:Int): List<Assignment> {

        return ArrayList<Assignment>()
    }

    fun getAssignmentsForStudent(student:Student): List<Assignment> {

        return ArrayList<Assignment>()
    }
}